import { NgModule } from '@angular/core';

import { TestSocialLoginSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [TestSocialLoginSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [TestSocialLoginSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class TestSocialLoginSharedCommonModule {}
