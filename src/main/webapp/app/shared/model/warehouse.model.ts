import { Moment } from 'moment';
import { IProduct } from 'app/shared/model/product.model';

export interface IWarehouse {
    id?: number;
    createAt?: Moment;
    updateAt?: Moment;
    description?: string;
    amount?: number;
    product?: IProduct;
}

export class Warehouse implements IWarehouse {
    constructor(
        public id?: number,
        public createAt?: Moment,
        public updateAt?: Moment,
        public description?: string,
        public amount?: number,
        public product?: IProduct
    ) {}
}
