import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IWarehouse } from 'app/shared/model/warehouse.model';
import { AccountService } from 'app/core';
import { WarehouseService } from './warehouse.service';

@Component({
    selector: 'jhi-warehouse',
    templateUrl: './warehouse.component.html'
})
export class WarehouseComponent implements OnInit, OnDestroy {
    warehouses: IWarehouse[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected warehouseService: WarehouseService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.warehouseService
            .query()
            .pipe(
                filter((res: HttpResponse<IWarehouse[]>) => res.ok),
                map((res: HttpResponse<IWarehouse[]>) => res.body)
            )
            .subscribe(
                (res: IWarehouse[]) => {
                    this.warehouses = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInWarehouses();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IWarehouse) {
        return item.id;
    }

    registerChangeInWarehouses() {
        this.eventSubscriber = this.eventManager.subscribe('warehouseListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
