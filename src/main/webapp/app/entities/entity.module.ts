import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'product',
                loadChildren: './product/product.module#TestSocialLoginProductModule'
            },
            {
                path: 'product',
                loadChildren: './product/product.module#TestSocialLoginProductModule'
            },
            {
                path: 'product-category',
                loadChildren: './product-category/product-category.module#TestSocialLoginProductCategoryModule'
            },
            {
                path: 'customer',
                loadChildren: './customer/customer.module#TestSocialLoginCustomerModule'
            },
            {
                path: 'product-order',
                loadChildren: './product-order/product-order.module#TestSocialLoginProductOrderModule'
            },
            {
                path: 'order-item',
                loadChildren: './order-item/order-item.module#TestSocialLoginOrderItemModule'
            },
            {
                path: 'invoice',
                loadChildren: './invoice/invoice.module#TestSocialLoginInvoiceModule'
            },
            {
                path: 'shipment',
                loadChildren: './shipment/shipment.module#TestSocialLoginShipmentModule'
            },
            {
                path: 'warehouse',
                loadChildren: './warehouse/warehouse.module#TestSocialLoginWarehouseModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestSocialLoginEntityModule {}
