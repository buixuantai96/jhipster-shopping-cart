package com.bui.tai.domain.enumeration;

/**
 * The InvoiceStatus enumeration.
 */
public enum InvoiceStatus {
    PAID, ISSUED, CANCELLED
}
